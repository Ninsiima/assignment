//import com.mongodb.*;
//
//import java.net.UnknownHostException;
//import java.util.Arrays;
//
//public class MongoDBInsertDocument {
//    public static void main(String[] args) throws UnknownHostException {
//        // Creates MongoDB client instance.
//        Clients client1=new Clients();
//
//        MongoClient client = new MongoClient(
//                new ServerAddress("localhost", 27017));
//
//        // Gets the school database from the MongoDB instance.
//        DB database = client.getDB("schools");
//
//        // Gets the teachers collection from the database.
//        DBCollection collection = database.getCollection("teachers");
//        collection.drop();
//
//        // Creates a document to be stored in the teachers collections.
//        DBObject document = new BasicDBObject("firstName", "John")
//                .append("lastName", "Doe")
//                .append("subject", "Computer Science")
//                .append("languages", Arrays.asList("Java", "C", "C++"))
//                .append("email", "john.doe@school.com")
//                .append("address",
//                        new BasicDBObject("street", "Main Apple St. 12")
//                                .append("city", "New York")
//                                .append("country", "USA"));
//
//        // Prints the value of the document.
//        System.out.println("document = " + document);
//
//        // Inserts the document into the collection in the database.
//        collection.insert(document);
//
//        // Prints the value of the document after inserted in the collection.
//        System.out.println("document = " + document);
//    }
//}