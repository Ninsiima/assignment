import com.mongodb.*;

import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.Scanner;

public class Clients{
//    String name,gender, location, district, country;
//    int age;

  public static void main(String[]args)  {
        Scanner input = new Scanner(System.in);
        String name, gender, location, district, country;
        String dateOfBirth;
//        double year;


      MongoClient client= new MongoClient();
      DB database = client.getDB("Clients");
      DBCollection collection =database.getCollection("ClientInfo");
//      DBObject document =new BasicDBObject("Clients name","Delis");
//      ((BasicDBObject) document).append("Location","Kikoni");
//      ((BasicDBObject) document).append("District","Kampala");
//      ((BasicDBObject) document).append("GeoCode","2,6");
//      ((BasicDBObject) document).append("Country","Uganda");
//      ((BasicDBObject) document).append("Age","20");
//      System.out.println(document);

//      System.out.println("document = " + document);


      DBObject record =new BasicDBObject();
     double latitude,longitude;
        System.out.print("Name:");
        name = input.next();
        ((BasicDBObject) record).append("name", name);
//      DBObject record1 =new BasicDBObject();
        System.out.print("Gender:");
        gender = input.next();
        ((BasicDBObject) record).append("gender",gender);

//      DBObject record3 =new BasicDBObject();
        System.out.print("Location:");
        location = input.next();
        ((BasicDBObject) record).append("location",location);

//      DBObject record4 =new BasicDBObject();
        System.out.print("District:");
        district = input.next();
        ((BasicDBObject) record).append("district",district);
//      DBObject record5 =new BasicDBObject();
        System.out.print("Country:");
        country = input.next();
        ((BasicDBObject) record).append("country",country);

//        System.out.print(":");


//        System.out.print("Year:");
//        year = input.nextDouble();


//      DBObject record6 =new BasicDBObject();
        System.out.print(" Latitude: ");
        latitude = input.nextDouble();
        ((BasicDBObject) record).append("latitude",latitude);

//      DBObject record7 =new BasicDBObject();
        System.out.print(" Longitude: ");
        longitude = input.nextDouble();
        ((BasicDBObject) record).append("longitude",longitude);
        System.out.print(" Geo code: ");
        System.out.println("("+latitude +","+longitude+")");
//        String[] parts = geoCode.split(",");
//

        System.out.print("Please enter date of birth in YYYY-MM-DD: ");
        dateOfBirth = input.next();
        LocalDate dob = LocalDate.parse(dateOfBirth);

        ((BasicDBObject) record).append("dateOfBirth",dateOfBirth);
        System.out.println("Age is:" + getAge(dob));
      collection.insert(record);


   System.out.println("document = " + record);

    }
    static int getAge(LocalDate dob) {

        LocalDate curDate = LocalDate.now();

      return Period.between(dob, curDate).getYears();

////import com.mongodb.*;
////
////import java.net.UnknownHostException;
////
//        class Test {
//            //            public static void main(String[] args) throws UnknownHostException {
//            public void getMongoInfo() {
//                MongoClient client = new MongoClient();
//                DB database = client.getDB("Client details");
//                DBCollection collection = database.getCollection("Client");
//                DBObject document = new BasicDBObject("Clients name", "Location");
//
//                System.out.println("document:" + document);
//            }
//        }
    }
}







